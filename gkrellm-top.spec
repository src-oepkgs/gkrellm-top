%define gkplugindir %{_libdir}/gkrellm2/plugins

Summary:	GKrellM plugin which shows 3 most CPU intensive processes
Name:		gkrellm-top
Version:	2.2.13
Release:	5%{?dist}
License:	GPL+
Group:		Applications/System
URL:		http://gkrelltop.sourceforge.net/
Source:		http://downloads.sf.net/gkrelltop/gkrelltop_%{version}.orig.tar.gz
Patch:		gkrelltop-2.2.13-optflags.patch
Requires:	gkrellm >= 2.2.0
BuildRequires:	gkrellm-devel >= 2.2.0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
A GKrellM plugin which displays the top three CPU intensive processes in a
small window inside GKrellM, similar to wmtop. Useful to check out anytime
what processes are consuming most CPU power on your machine.

%prep
%setup -q -n gkrelltop-%{version}.orig
%patch -p1 -b .optflags

%build
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
install -D -m 755 gkrelltop.so $RPM_BUILD_ROOT%{gkplugindir}/gkrelltop.so
install -D -m 755 gkrelltopd.so $RPM_BUILD_ROOT%{gkplugindir}/gkrelltopd.so

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc README
%{gkplugindir}/gkrelltop.so
%{gkplugindir}/gkrelltopd.so

%changelog
* Wed Feb 13 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.2.13-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Thu Jul 19 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.2.13-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Fri Jan 13 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.2.13-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.2.13-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sat Jul 25 2009 Robert Scheck <robert@fedoraproject.org> 2.2.13-1
- Upgrade to 2.2.13

* Mon Feb 23 2009 Robert Scheck <robert@fedoraproject.org> 2.2.11-2
- Rebuild against gcc 4.4 and rpm 4.6

* Sun Feb 10 2008 Robert Scheck <robert@fedoraproject.org> 2.2.11-1
- Upgrade to 2.2.11

* Wed Dec 12 2007 Robert Scheck <robert@fedoraproject.org> 2.2.10-1
- Upgrade to 2.2.10
- Initial spec file for Fedora and Red Hat Enterprise Linux
